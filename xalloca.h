/*
 * This file is part of xalloca.
 * 
 * Copyright (C) 2014 Karl Lindén <lilrc@users.sourceforge.net>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

#ifndef XALLOCA_H
# define XALLOCA_H

# if HAVE_CONFIG_H
#  include <config.h>
# endif /* HAVE_CONFIG_H */

# if ENABLE_ALLOCA
#  include <alloca.h>
#  define xalloca alloca
#  define xfreea(ptr) /* nothing */
# else /* !ENABLE_ALLOCA */
#  include <stdlib.h>
#  define xalloca malloc
#  define xfreea free
# endif /* !ENABLE_ALLOCA */

#endif /* !XALLOCA_H */
